Dependecy of vacancy diffusivity (migration of all atom at diffusion of single vacancy) on temperature; Molecular dynamics; ADP potential [S. Starikov. D. Smirnova]

1/T [1/K]	D [10^{-8} m2/s]
 

0.00141	0.0012
0.00125	0.004
0.00115	0.008
0.00105	0.017
0.00099	0.024
0.00095	0.035