Dependecy of vacancy diffusivity (migration of all atom at diffusion of single vacancy) on temperature; Molecular dynamics + SD; ADP potential [S. Starikov. D. Smirnova]

1/T [1/K]	D [10^{-8} m2/s]

0.00121	0.0067
0.00118	0.0079
0.0015	0.00075
0.00143	0.0011
0.00123	0.0059
0.001	0.031
 
